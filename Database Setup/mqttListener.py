
import paho.mqtt.client as mqtt
import time

username="capstone"
password="raspberry"
broker="rest.alteredcloud.com"
port=8883

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, message):
    print("message received " ,str(message.payload.decode("utf-8")))
    print("message topic",message.topic)
    if message.topic == "insert/d1" || message.topic == "insert/d2" || message.topic == "insert/d3" || message.topic == "insert/d4":
        p=str(message.payload.decode("utf-8"))

def on_log(client, userdata, level, buf):
    print("log: "+buf)
#initialize client
client = mqtt.Client()

#bind callback functions
client.on_connect = on_connect
client.on_message = on_message
client.on_log = on_log
#set TLS and authentication up
client.tls_set('/etc/ssl/certs/ca-certificates.crt')
client.username_pw_set(username, password)

#connect to the broker
print("Connecting to broker at "+broker)
client.connect(broker, port, 60)

client.subscribe("test")
# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
client.loop_forever()
