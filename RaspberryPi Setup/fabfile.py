# python file used by fabric
from fabric import task

# testing purposes
@task
def hello(c):
    c.run('echo Hello World')

# print current working directory across each raspberry pi
@task
def printWorkingDirectory(c):
    hostname = c.run('hostname').stdout.strip()
    dir = c.run('pwd').stdout.strip()
    print('Current working directory for {0} is {1}'.format(hostname, dir))
    
def shutDown(c):
        c.run('sudo shutdown -h now')

@task
def capture(c):
        c.run('cd locatorsrepo/RaspberryPi\ Setup/ && sudo sh capture.sh')

@task
def begin(c):
        c.run('cd locatorsrepo/RaspberryPi\ Setup/ && sudo sh rfMon.sh')
        
@task
def mqttDemo(c):
        c.run('cd locatorsrepo/RaspberryPi\ Setup/ && sudo python3 mqttConnect.py')
        
@task
def update(c):
        c.run('sudo apt update')
        c.run('sudo apt install tshark -y')
        c.run('sudo apt install python3-pip -y')
        c.run('sudo pip install paho-mqtt -y')
        c.run('sudo git clone https://zachary_gold@bitbucket.org/emergencypingadelphi/locatorsrepo.git')
        
