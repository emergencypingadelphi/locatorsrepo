#!/bin/bash

ifconfig mon0 up
touch tshark.txt
tshark -i mon0 -a packets:1 -V wlan type mgt subtype probe-req > tshark.txt
ifconfig mon0 down
